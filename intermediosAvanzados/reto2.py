'''
    ---------- RETO 2 Avanzado --------------
    AUTOR: MARTÍN SAN JOSÉ DE VICENTE
    EMAIL: martin@imaginagroup.com
    AÑO: 2021
    LICENCIA CÓDIGO: OSS
    ------------------------------------------
'''

'''
El programa debe preguntar el artículo y su precio y añadirlo a una variable (diccionario u objeto literal), 
hasta que el usuario decida terminar ("Introducir otro elemento al carrito (Si / No)".

Una vez el usuario decida no introducir más elementos al carrito, debe mostrar por pantalla la lista de la compra y el coste total.
'''